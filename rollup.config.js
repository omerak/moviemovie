import buble from 'rollup-plugin-buble';
import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';



export default {
  entry: 'src/index.js',
  plugins: [
    buble(),

    replace({
      'process.env.NODE_ENV': '"development"'
    }),

    commonjs({
      include: 'node_modules/**'
    }),

    nodeResolve({ browser: true, jsnext: false, main: true })
  ],
  dest: 'dist/bundle.js',
};
