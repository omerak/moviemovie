var path = require('path');
var express = require('express');

var app = express();


app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.use("/dist/", express.static(__dirname + '/dist/'));

app.listen(3000, 'localhost', function(err) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('go to http://localhost:3000');
});
