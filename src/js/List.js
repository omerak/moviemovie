import React from 'react';

import { urls } from './Constants';
import { search } from './mixin';


export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: []
    };
  }

  componentDidMount() {
    search.bind(this)(this.props.query);
  }

  renderMovie(movie) {
    return (
      <a
        href={urls.imdb + movie.imdbID}
        target="_blank"
        key={movie.imdbID}
        className="list__item"
        style={{ backgroundImage: 'url(' + movie.Poster +')'}}
      >
        <div className="list__item__title">
          {movie.Title}
        </div>
      </a>
    );
  }

  render() {
    return (
      <div className="list" ref="html">
        <a href="#/" className="list__search">Tekrar Ara</a>
        <div/>
        {this.state.movies.map(this.renderMovie)}
      </div>
    );
  }
}
