import queryString from 'query-string';

import { urls } from './Constants';


export function search(query, that) {
  fetch(urls.omdb + queryString.stringify({
    s: query,
    r: 'json'
  })).then(function(response) {
    return response.json();
  }).then(function(response) {
    this.setState({
      movies: response.Search || []
    })
  }.bind(this));
}
