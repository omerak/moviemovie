export const urls = {
  omdb: 'http://www.omdbapi.com/?',
  imdb: 'http://www.imdb.com/title/'
};
export const autocompleteLimit = 2;
