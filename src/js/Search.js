import React from 'react';

import { urls, autocompleteLimit } from './Constants';
import { search } from './mixin';

import Autocomplete from  'react-autocomplete';


export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      movies: []
    };
  }

  onChange(e) {
    this.setState({value: e.target.value}, function() {
      this.search();
    });
  }

  search() {
    if (this.state.value.length > autocompleteLimit) {
      search.bind(this)(this.state.value);
    }
  }

  onSelect(value, item) {
    window.open(urls.imdb + item.imdbID, '_blank');
  }

  onSubmit(e) {
    e.preventDefault();
    if (this.state.value.length > autocompleteLimit)
      location.href = '#/list/' + this.state.value;
  }

  render() {
    return (
      <form className="search" onSubmit={this.onSubmit.bind(this)}>
        <div className="search__input">
          <Autocomplete
            value={this.state.value}
            items={this.state.movies}
            onSelect={this.onSelect.bind(this)}
            onChange={this.onChange.bind(this)}
            getItemValue={(item) => item.Title}
            renderItem={(item, isHighlighted) => (
              <div
                className={isHighlighted ? "search-item--highlighted" : ""}
                key={item.imdbID}
                id={item.imdbID}
              >{item.Title}</div>
            )}
          />
        </div>
        <button className="search__button" type="submit">Ara</button>
      </form>
    );
  }
}
