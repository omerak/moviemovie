import React from 'react';

import router from './router';


export default class App extends React.Component {
  constructor() {
    this.state = {
      component: <span></span> 
    };
  }

  componentDidMount() {
    router(this._routeChange.bind(this));
  }

  _routeChange(route) {
    this.setState({component: route});
  }

  render() {
    return (
      <div className="root">
        {this.state.component}
      </div>
    );
  }
}
