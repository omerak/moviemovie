import director from 'director';
import React from 'react';

import Search from './Search';
import List from './List';


class routerContainer {
  constructor(callback) {
    if (!location.hash) {
      location.hash = '#/';
    }

    var route;

    var search = function () {
      route = <Search key="search"/>
    };

    var list = function (query) {
      route = <List key="list" query={query}/>
    };

    var routes = {
      '/': search,
      '/search': search,
      '/list/:q': list,
    };

    var allroutes = function() {
      callback(route);
    };

    var router = director.Router(routes);

    router.configure({
      on: allroutes
    });

    router.init();
  }
};


export default routerContainer;
